# SPOGGER
[![CI status](https://gitlab.com/tslocum/spogger/badges/master/pipeline.svg)](https://gitlab.com/tslocum/spogger/commits/master)
[![Donate](https://img.shields.io/liberapay/receives/rocketnine.space.svg?logo=liberapay)](https://liberapay.com/rocketnine.space)

Frogger rip-off ([Unexpected Jam](https://itch.io/jam/unexpectedjam) submission)

## Play

```bash
go get gitlab.com/tslocum/spogger # Download
spogger # Run
```

## Support

Please share issues and suggestions [here](https://gitlab.com/tslocum/spogger/issues).
