module gitlab.com/tslocum/spogger

go 1.15

require (
	github.com/gdamore/tcell v1.4.0
	github.com/gdamore/tcell/v2 v2.0.0-dev.0.20200831155531-da485f47343b
	gitlab.com/tslocum/cview v1.4.9-0.20200901224443-f35f86924a49
)
