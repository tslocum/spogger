package main

import (
	"log"

	"time"

	"strings"

	"gitlab.com/tslocum/cview"
)

const (
	appWidth  = 120
	appHeight = 38
)

const (
	screenTitle = 0
)

const titleScreenText = `




                          ███████  ██████    ██████    ██████    ██████   ███████  ██████  
                          ██       ██   ██  ██    ██  ██        ██        ██       ██   ██ 
                          ███████  ██████   ██    ██  ██   ███  ██   ███  █████    ██████  
                               ██  ██       ██    ██  ██    ██  ██    ██  ██       ██   ██ 
                          ███████  ██        ██████    ██████    ██████   ███████  ██   ██ 

                    


                                                                           
                                                                          
                                                                     
                                                                    
                                                                   
                                                                  



                     x
                     |
                     |                                     .
                  =======                                 / \
                   =====                                 / = \
              ===============                           / = = \
          =======================                       |=|=|=| 
             =================                          |=|=|=|
            \\\\\\|||||||//////                         |=|=|=|                                  ___
              | |    |    | |                           |=|=|=|                                 /** \
               | |   |   | |                       _____|=|=|=|                            _   /***  \
                | |  |  | |                       |=|=|=|=|=|=|                           /*\_/***    \
                 | | | | |                        |=|=|=|=|=|=|                          /##########   \
                  | ||| |                         |=|=|=|=|=|=|                         /############   \
`

var (
	app *cview.Application

	initialResizeTriggered bool

	currentScreen int

	titleScreenTextView *cview.TextView
)

func handleResize(width, height int) {
	if !initialResizeTriggered {
		if width < appWidth || height < appHeight {
			app.Stop()
			log.Fatalf("failed to start SPOGGER: minimum terminal size is %dx%d, current size is %dx%d", appWidth, appHeight, width, height)
		}

		initialResizeTriggered = true
	}
}

func containerGrid(p cview.Primitive) *cview.Grid {
	g := cview.NewGrid().
		SetColumns(-1, appWidth, -1).
		SetRows(-1, appHeight, -1)

	g.AddItem(cview.NewTextView(), 0, 0, 1, 3, 0, 0, false)
	g.AddItem(cview.NewTextView(), 1, 0, 1, 1, 0, 0, false)
	g.AddItem(p, 1, 1, 1, 1, 0, 0, false)
	g.AddItem(cview.NewTextView(), 1, 2, 1, 1, 0, 0, false)
	g.AddItem(cview.NewTextView(), 2, 0, 1, 3, 0, 0, false)

	return g
}

func titleScreen() *cview.Grid {
	return containerGrid(titleScreenTextView)
}

func setScreen(screen int) {
	switch screen {
	case screenTitle:
		app.SetRoot(titleScreen(), true)

		app.Draw()
	}
}

func updateTitleScreen() {
	for {
		titleScreenTextView.SetText(strings.ReplaceAll(titleScreenText[1:], "x", ""))
		app.Draw()

		time.Sleep(2 * time.Second)

		titleScreenTextView.SetText(strings.ReplaceAll(titleScreenText[1:], "x", "."))
		app.Draw()

		time.Sleep(2 * time.Second)
	}
}

func main() {
	app = cview.NewApplication()

	titleScreenTextView = cview.NewTextView()
	titleScreenTextView.SetBorder(true)

	go updateTitleScreen()

	app.EnableMouse(true)

	app.SetAfterResizeFunc(handleResize)

	go updateTitleScreen()

	setScreen(screenTitle)

	if err := app.Run(); err != nil {
		log.Fatal(err)
	}
}
